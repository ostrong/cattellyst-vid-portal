// ignore: depend_on_referenced_packages
import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:flutter/material.dart';

import 'home/home.dart';

void main() {
  setUrlStrategy(PathUrlStrategy());
  runApp(const App());
}

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final List<Page> _pages = [];

  void _addPage(Page newPage) {
    setState(() {
      _pages.add(newPage);
    });
  }

  @override
  void initState() {
    super.initState();
    _addPage(MaterialPage(
      key: const ValueKey('VacationDestinationsList'),
      child: HomePage(
        title: 'Cattellyst Media Management',
        callback: _addPage,
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cattellyst Media Management',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Navigator(
        pages: List.unmodifiable(_pages),
        onPopPage: (route, result) {
          if (!route.didPop(result)) {
            return false;
          }

          setState(() {
            _pages.removeLast();
          });

          return true;
        },
      ),
    );
  }
}
